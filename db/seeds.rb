# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
users = [
  [ "usuario1@test.com", 'password1' ],
  [ "usuario2@test.com", 'password2' ],
  [ "usuario3@test.com", 'password3' ]
]

users.each do |email, password|
  User.create( email: email, password: password, password_confirmation: password )
end

c1 = Course.create(year: 2016)
c2 = Course.create(year: 2017)

students = [
  [ "Gonzalez", 'Pedro', '11111111', '9876/3', 'pedro@test.com', c1 ],
  [ "Rodriguez", 'Pablo', '2222222', '9837/3', 'pablo@test.com', c1 ],
  [ "Perez", 'German', '33333333', '4593/3', 'german@test.com', c1 ],
  [ "Gomez", 'Rodrigo', '44444444', '4453/3', 'rodrigo@test.com', c1 ],
  [ "Basualdo", 'Augusto', '55555555', '2346/3', 'augusto@test.com', c1 ],
  [ "Landa", 'Agustin', '6667676', '5326/3', 'agustin@test.com', c2 ],
  [ "Romero", 'Nicolas', '12345678', '0834/3', 'nico@test.com', c2 ],
  [ "Gimenez", 'Pedro', '9999999', '1245/3', 'pedrito@test.com', c2 ],
  [ "Vasquez", 'Pablo', '111112222', '24523/3', 'pablito@test.com', c2 ],
  [ "Soldan", 'Roberto', '3333344444', '24124/3', 'robertito@test.com', c2 ]
]

students.each do |last, first, doc, file, mail, course|
  Student.create( email: mail, last_name: last, first_name: first, documet: doc, file: file, course: course )
end

tests = [
  [ "Evaluacion 1 2016", Date.new(2016,02,04), 4, c1 ],
  [ "Evaluacion 2 2016", Date.new(2016,03,07), 5, c1 ],
  [ "Evaluacion 1 2017", Date.new(2017,02,04), 5, c2 ],
  [ "Evaluacion 2 2017", Date.new(2017,03,07), 7, c2 ]
]

tests.each do |title, date, minimum, course|
  Test.create( title: title, date: date, minimum_score: minimum, course: course )
end

StudentTest.create(test: Test.first, student: Student.first, score:5)
StudentTest.create(test: Test.last, student: Student.last, score:9)