class CreateTests < ActiveRecord::Migration[5.1]
  def change
    create_table :tests do |t|
      t.string :title
      t.datetime :date
      t.integer :minimum_score
      t.belongs_to :course, foreign_key: true

      t.timestamps
    end
  end
end
