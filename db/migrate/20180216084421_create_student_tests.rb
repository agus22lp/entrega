class CreateStudentTests < ActiveRecord::Migration[5.1]
  def change
    create_table :student_tests do |t|
      t.belongs_to :student, foreign_key: true
      t.belongs_to :test, foreign_key: true
      t.integer :score

      t.timestamps
    end
  end
end
