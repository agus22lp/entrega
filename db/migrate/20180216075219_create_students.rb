class CreateStudents < ActiveRecord::Migration[5.1]
  def change
    create_table :students do |t|
      t.string :first_name
      t.string :last_name
      t.string :file
      t.string :documet
      t.string :email
      t.belongs_to :course, foreign_key: true

      t.timestamps
    end
  end
end
