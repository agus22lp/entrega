json.extract! test, :id, :title, :date, :minimum_score, :course_id, :created_at, :updated_at
json.url test_url(test, format: :json)
