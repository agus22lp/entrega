class Test < ApplicationRecord
  belongs_to :course
  has_many :students, through: :course
  has_many :student_tests, :dependent => :delete_all
  validates :title, :date, :course, presence: true

  def approved? student_test
  	student_test.score >= minimum_score
  end
end
