class Course < ApplicationRecord
	has_many :tests, :dependent => :delete_all
	has_many :students, :dependent => :delete_all
	before_destroy :delete_notes

	validates :year, presence: true
	validates :year, uniqueness: true

	private

	def delete_notes
		StudentTest.where(test_id: test_ids).destroy_all
	end
end
