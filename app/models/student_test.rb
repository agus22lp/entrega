class StudentTest < ApplicationRecord
  belongs_to :student
  belongs_to :test
  validates :student, :test, presence: true
end
