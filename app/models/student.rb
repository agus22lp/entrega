class Student < ApplicationRecord
  belongs_to :course
  has_many :student_tests, :dependent => :delete_all
  validates :first_name, :last_name, :documet, :file, presence: true
end
