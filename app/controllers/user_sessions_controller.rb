# app/controllers/user_sessions_controller.rb
class UserSessionsController < ApplicationController
	skip_before_action :require_login, except: [:destroy]

  def new
    @user = User.new
  end

  def create
    if @user = login(params[:email], params[:password])
      redirect_back_or_to(:courses, notice: 'Usuario logueado')
    else
      flash.now[:alert] = 'Login invalido'
      render action: 'new'
    end
  end

  def destroy
    logout
    redirect_to(login_path, notice: 'Usuario deslogueado!')
  end
end