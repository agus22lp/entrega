class TestsController < ApplicationController
  before_action :set_test, only: [:show, :edit, :update, :destroy,:scores,:update_scores]
  before_action :set_course

  # GET /tests
  # GET /tests.json
  def index
    @tests = @course.tests.order(:date).all
  end

  # GET /tests/1
  # GET /tests/1.json
  def show
  end

  # GET /tests/new
  def new
    @test = @course.tests.build
  end

  # GET /tests/1/scores
  def scores
    @students = @test.students.order(:last_name).all
    @scores = StudentTest.where(test_id: @test.id)
  end

  def update_scores
    student_scores = params["student_score"]

    ActiveRecord::Base.transaction do
      StudentTest.where(test_id: @test.id).destroy_all
      student_scores.each do |st_sc|
        if (student_scores[st_sc]["score"].to_s != "")
          StudentTest.create(test_id: @test.id, student_id: student_scores[st_sc]["id"], score: student_scores[st_sc]["score"])
        end
      end
    end
    respond_to do |format|
      format.html { redirect_to scores_course_test_path(@course, @test) , notice: 'Las notas fueron guardadas.' }
    end
    
  end

  # GET /tests/1/edit
  def edit
  end

  # POST /tests
  # POST /tests.json
  def create
    @test = @course.tests.create(test_params)

    respond_to do |format|
      if @test.save
        format.html { redirect_to course_tests_path(@course) , notice: 'La evaluacion fue creada.' }
        format.json { render :show, status: :created, location: @test }
      else
        format.html { render :new }
        format.json { render json: @test.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tests/1
  # PATCH/PUT /tests/1.json
  def update
    respond_to do |format|
      if @test.update(test_params)
        format.html { redirect_to course_tests_path(@course), notice: 'La evaluacion fue actualizada.' }
        format.json { render :show, status: :ok, location: @test }
      else
        format.html { render :edit }
        format.json { render json: @test.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tests/1
  # DELETE /tests/1.json
  def destroy
    @test.destroy
    respond_to do |format|
      format.html { redirect_to course_tests_path(@course), notice: 'La evaluacion fue eliminada.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_test
      @test = Test.find(params[:id])
    end

    def set_course
      @course = Course.find(params[:course_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def test_params
      params.require(:test).permit(:title, :date, :minimum_score, :course_id)
    end
end
