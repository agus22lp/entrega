require 'test_helper'

class TestTest < ActiveSupport::TestCase
  test "user is approved" do
  	course = Course.create(year: 2017)
  	test1 = Test.create(minimum_score: 5, title: 'Ev 1', date: Date.today)
  	student1 = Student.create( email: 'mail', last_name: 'last', first_name: 'first', documet: 'doc', file: 'file', course: course )
  	student_t1 = StudentTest.create(score: 10, student: student1, test: test1)
  	assert test1.approved? student_t1
  end
  test "user is disapproved" do
  	course = Course.create(year: 2017)
  	test2 = Test.create(minimum_score: 5, title: 'Ev 2', date: Date.yesterday)
  	student2 = Student.create( email: 'mail', last_name: 'last', first_name: 'first', documet: 'doc', file: 'file', course: course )
  	student_t2 = StudentTest.create(score: 2, student: student2, test: test2)
  	assert !test2.approved?(student_t2)
  end
  test "user is approved with minimum score" do
  	course = Course.create(year: 2017)
  	test3 = Test.new(minimum_score: 5, title: 'Ev 3', date: Date.tomorrow)
  	student3 = Student.create( email: 'mail', last_name: 'last', first_name: 'first', documet: 'doc', file: 'file', course: course )
  	student_t3 = StudentTest.create(score: 5, student: student3, test: test3)
  	assert test3.approved? student_t3
  end

end
