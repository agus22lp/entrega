require 'test_helper'

class CourseTest < ActiveSupport::TestCase
  test "course has ten students" do
    c = Course.create(year: 2017)
    values = [
	  [ "Gonzalez", 'Pedro', '11111111', '9876/3', 'pedro@test.com', c ],
	  [ "Rodriguez", 'Pablo', '2222222', '9837/3', 'pablo@test.com', c ],
	  [ "Perez", 'German', '33333333', '4593/3', 'german@test.com', c ],
	  [ "Gomez", 'Rodrigo', '44444444', '4453/3', 'rodrigo@test.com', c ],
	  [ "Basualdo", 'Augusto', '55555555', '2346/3', 'augusto@test.com', c ],
	  [ "Landa", 'Agustin', '6667676', '5326/3', 'agustin@test.com', c ],
	  [ "Romero", 'Nicolas', '12345678', '0834/3', 'nico@test.com', c ],
	  [ "Gimenez", 'Pedro', '9999999', '1245/3', 'pedrito@test.com', c ],
	  [ "Vasquez", 'Pablo', '111112222', '24523/3', 'pablito@test.com', c ],
	  [ "Soldan", 'Roberto', '3333344444', '24124/3', 'robertito@test.com', c ]
		]
		students = []
		values.each do |last, first, doc, file, mail, course|
		  students <<  Student.create( email: mail, last_name: last, first_name: first, documet: doc, file: file, course: course )
		end
    c.students.append(students)
    c.save
    assert c.students.count == 10
  end

  test "course is valid" do
	  hash = { year: 2017 }

	  course = Course.create hash

	  assert course.valid?
  end

  test "course is invalid" do
	  hash = { year: 2017 }

	  course = Course.create hash
	  course1 = Course.create hash

	  assert !course1.valid?
  end

end
