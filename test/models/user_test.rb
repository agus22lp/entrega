require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "user is invalid" do
	  hash = { email: 'test@test.com',
	        password: '1' }

	  user = User.new hash

	  assert !user.valid?
  end

  test "user is valid" do
	  hash = { email: 'test2@test.com',
	        password: '12345678', password_confirmation: '12345678' }

	  user = User.new hash

	  assert user.valid?
  end

end
