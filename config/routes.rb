Rails.application.routes.draw do
	resources :user_sessions
	root :to => 'courses#index'
	get 'login' => 'user_sessions#new', :as => :login
	post 'logout' => 'user_sessions#destroy', :as => :logout
	resources :courses do
		member do
			get "results"
			get "statistics"
		end
  	resources :tests do
  		member do
  			get "scores"
  			post "scores", to: 'tests#update_scores'
  		end
  	end
  	resources :students
	end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
